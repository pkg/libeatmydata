Description: Use a weak reference to pthread_testcancel, so that it's called only if the program itself pulls libpthread
 libeatmydata is linked against libpthread. This thus pulls libpthread
 in all processes run under eatmydata. It happens that having
 libpthread in a process makes pthread_mutex_lock() and all such basic
 performance-sensitive operations much heavier (since they then have to
 deal with potential multithreading). This avoids that by using a weak reference
 to pthread_testcancel, so that pthread_testcancel is called only if the program
 itself pulls libpthread.
Author: Samuel Thibault <sthibault@debian.org>
Bug: https://bugs.launchpad.net/libeatmydata/+bug/1556410
Bug-Debian: https://bugs.debian.org/816144
Forwarded: https://github.com/stewartsmith/libeatmydata/pull/20
Last-Update: 2020-04-28

--- a/libeatmydata/libeatmydata.c
+++ b/libeatmydata/libeatmydata.c
@@ -67,6 +67,7 @@
         libc_##name = (libc_##name##_##t)(intptr_t)dlsym(RTLD_NEXT, #name);			\
 						   dlerror();
 
+#pragma weak pthread_testcancel
 
 int LIBEATMYDATA_API msync(void *addr, size_t length, int flags);
 static int initing = 0;
@@ -113,7 +114,8 @@
 int LIBEATMYDATA_API fsync(int fd)
 {
 	if (eatmydata_is_hungry()) {
-		pthread_testcancel();
+		if (pthread_testcancel)
+			pthread_testcancel();
 		errno= 0;
 		return 0;
 	}
@@ -188,7 +190,8 @@
 int LIBEATMYDATA_API fdatasync(int fd)
 {
 	if (eatmydata_is_hungry()) {
-		pthread_testcancel();
+		if (pthread_testcancel)
+			pthread_testcancel();
 		errno= 0;
 		return 0;
 	}
@@ -199,7 +202,8 @@
 int LIBEATMYDATA_API msync(void *addr, size_t length, int flags)
 {
 	if (eatmydata_is_hungry()) {
-		pthread_testcancel();
+		if (pthread_testcancel)
+			pthread_testcancel();
 		errno= 0;
 		return 0;
 	}
@@ -211,7 +215,8 @@
 int sync_file_range(int fd, off64_t offset, off64_t nbytes, unsigned int flags)
 {
 	if (eatmydata_is_hungry()) {
-		pthread_testcancel();
+		if (pthread_testcancel)
+			pthread_testcancel();
 		errno= 0;
 		return 0;
 	}
--- a/Makefile.am
+++ b/Makefile.am
@@ -80,7 +80,7 @@
   $(NO_WERROR) \
 	-DBUILDING_LIBEATMYDATA
 
-libeatmydata_la_LIBADD = $(LIBDL_LIBS) -lpthread
+libeatmydata_la_LIBADD = $(LIBDL_LIBS)
 libeatmydata_la_LDFLAGS = $(AM_LDFLAGS) -version-info $(LIBEATMYDATA_VERSION)
 
 #install-exec-hook:
