2014-10-30  Mattia Rizzolo  <mattia@mapreri.org>

	Fix executable-not-elf-or-script lintian warning

2014-10-30  Mattia Rizzolo  <mattia@mapreri.org>

	Fix multiarch issue
	
	Debian bug: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=765810

2014-10-30  Stewart Smith  <stewart@flamingspork.com>

	merge remove install-exec hook

2013-11-20  Stewart Smith  <stewart@flamingspork.com>

	remove install-exec-hook that removed the .la lib as this broke make uninstall and make distcheck

2014-10-30  Stewart Smith  <stewart@flamingspork.com>

	update AUTHORS

2014-10-30  Stewart Smith  <stewart@flamingspork.com>

	Some test suite updates.
	
	Add tst-key4 from libc, although we need to deal with 1 less pthread key
	than normal due to something that's going on with pthread when libeatmydata
	is present that I don't quite know what's going on.
	
	We also update tst-cancel and the test run script so that we better capture
	errors from the test suite.

2014-10-30  Mattia Rizzolo  <mattia@mapreri.org>

	open() returns -EFAULT when program doesn't run ctor
	
	https://bugs.launchpad.net/libeatmydata/+bug/1374862
	
	bug forwarded from debian, see the remote tracker.
	
	I encountered some mysterious errors with eatmydata. For example, when installing gnunet-server (as a build dependency of some source package), the following error happens:
	
	Fatal: can't open /dev/urandom: Bad address
	
	In fact, you can easily reproduce this (also on other arches like amd64) with:
	
	# eatmydata gnunet-arm
	Fatal: can't open /dev/urandom: Bad address
	Aborted
	#
	
	This is due to the assumption that normally, eatmydata_init() is called as a constructor before main():
	
	void __attribute__ ((constructor)) eatmydata_init(void)
	
	However, some programs like gnunet-arm for some reason don't run ctors of our SO's ctor eatmydata_init(). While it might be a good idea to fix this in gnunet also, I propose a workaround in eatmydata to even run programs like gnunet-arm
	with this issue more transparently (as run without eatmydata).
	
	Consider the attached patch: It fixes the issue by detecting directly if we are just in the process of eatmydata_init() already. This is already done by checking for !libc_open but this also happens in the above case: eatmydata_init() not been called previously, leading to the assumption in open() that we are catching the case of just initing and coming back from
	dlsym() to open(). But here, this branch catches the wrong state, because in gnunet-arm, eatmydata_init() wasn't called at all.
	
	So the patch issues EFAULT only in case of actually just eatmydata_init()ing. Otherwise, eatmydata_is_hungry() and therefore eatmydata_init() can be called just as usual.
	
	FWIW, the case of eatmydata_init() not being run as a ctor is already handled by running it in eatmydata_is_hungry() but this doesn't work in case of open() being called before one of the other triggering functions.
	
	Note that gnunet-server is just an example. I encountered the same issue in some other occasions also.
	
	Furthermore, this bug affects the new release of gnutls, and thus every package that use it. This makes impossible to use gnutls with eatmydata.
	
	Please consider the patch attached, which is tested by various people and soon will be uploaded to Debian.

2014-10-30  Stewart Smith  <stewart@flamingspork.com>

	update copyright headers

2014-10-30  Stewart Smith  <stewart@flamingspork.com>

	whitespace cleanup

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove traces of sphinx from build

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove pandora_run_cpplint

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove traces of C++ from autotools/pandora

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove pandora sasl

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove pandora check for gcc atomics

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove pandora check for clock_gettime

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	remove some unused pandora files

2013-11-19  Stewart Smith  <stewart@flamingspork.com>

	update automake test options so that we function in most places correctly, using parallel test run if possible

2013-08-09  Stewart Smith  <stewart@flamingspork.com>

	sync() isn't a cancellation point - per Aurelien Jarno in debian bug 713035

2013-08-09  Jaroslav Kortus  <jkortus@redhat.com>

	Update SPEC file to meet Fedora packaging guidelines. Thanks to Jaroslav Kortus for the patch.

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	last bunch of rpm package fixes, along with some makefile fixes. Don't do versioned .so anymore, don't package .la, don't build static libs, fix up rpm spec for all supported RPM distros that build.opensuse.org builds

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	fix up GPL-3 license string

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	fix up some spec file things: don't autoreconf, it's unneeded and breaks on old distros. Also, capitalize summary (rpmlint)

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	update spec file to be configured correctly with version and be buildable by build.opensuse.org

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	bump version number for release

2013-06-28  Stewart Smith  <stewart@flamingspork.com>

	Add pthread_testcancel() calls to fix a bug where libeatmydata wrapped fsync()
	wasn't a cancellation point which caused problems with eglibc builds in debian.
	
	I've added (and slightly modified) a test case from eglibc as provided by Thomas Preud'homme
	in Debian bug 713035.

2013-05-21  Stewart Smith  <stewart@flamingspork.com>

	another hack to get open and open64 on 32bit

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	bump version number to 79, do release

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	only include open64 if __USE_FILE_OFFSET64 is not defined. I think this ends up being okay everywhere (works on a 64bit and a 32bit machine at least)

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	first go at fixing the open() vs open64() problem: duplicate the open() wrapper for open64()

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	add test case that does the same eatmydatatest.c but with #define _FILE_OFFSTE_BITS 64 

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	*cough* actually do test_run exit code correctly

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	should clear errno before checking it in TESTCASE macro

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	fix test_run.sh logic for return code from tests

2013-05-17  Stewart Smith  <stewart@flamingspork.com>

	fix up test_run.sh to get the return code of the grep correct

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	update AUTHORS, debian (C) and ensure that packaging makes it into source dist

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	remove whitespace in build_emd_rpm.sh

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	delete debian/README.source - is unneeded

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	add proper README.Debian

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	update (C) in README

2013-05-14  Stewart Smith  <stewart@flamingspork.com>

	merge lp:~abychko/libeatmydata/libeatmydata-packaging

2013-05-13  Alexey Bychko  <alexey.bychko@percona.com>

	added deb and rpm build files

2012-09-26  Stewart Smith  <stewart@flamingspork.com>

	bump version number for release

2012-09-26  Stewart Smith  <stewart@flamingspork.com>

	add Blair Zajac <blair@imageworks.com> to AUTHORS file, thanks for the MacOS X 10.7 support

2012-09-26  Blair Zajac  <blair@imageworks.com>

	Silence compiler warning on Mac OS X which doesn't
	declare fdatasync().

2012-09-26  Blair Zajac  <blair@imageworks.com>

	Fix compile on Mac OS X which doesn't have
	sync_file_range().

2012-08-27  Stewart Smith  <stewart@flamingspork.com>

	update (C) year in libeatmydata.c

2012-08-27  Stewart Smith  <stewart@flamingspork.com>

	update (C) year in AUTHORS

2012-07-05  Stewart Smith  <stewart@flamingspork.com>

	bump version number for release

2012-07-05  Stewart Smith  <stewart@flamingspork.com>

	fix up Darwin/Linux detection in eatmydata script

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	add Phillip Susi to authors as the sync_file_range patch came from him

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	bump version number for release

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	remove unneeded pandora files

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge pandora update

2011-05-17  Stewart Smith  <stewart@flamingspork.com>

	pandora update

2011-05-17  Monty Taylor  <mordred@inaugust.com>

	Removed autogen.sh. Either use config/autorun.sh or autoreconf -fi

2011-05-17  Monty Taylor  <mordred@inaugust.com>

	Updated pandora-build files to version 0.176

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge r29

2011-05-17  Stewart Smith  <stewart@flamingspork.com>

	remove extraneous configure.in

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	null merge r28

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	change version number to 28 ready for release

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	merge autotools foo: using pandora-build. This should all mostly keep everything a-okay

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge old makefile make dist target

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	fix make dist target

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	null merge old auto-foo

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	update version in configure.in

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge portable test suite from debian package, updating for new packaging and fixing compiler warningS

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	really add the portable test suite source

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge debian lenny fix: sys/mman.h required in fsynctest.c

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	merge Debian lenny support

2010-12-15  olly@survex.com

	Add explicit '#include <sys/mman.h>' for msync, needed on Debian lenny.

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge install target of old makefile

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	merge primitive make install target

2010-10-28  Elliot Murphy  <elliot@elliotmurphy.com>

	added install target

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge and fix support for Debian GNU/kFreeBSD - namely mode_t and int size check/difference

2011-01-04  Modestas Vainius  <modax@debian.org>

	import handle_va_arg_promotion patch from Debian
	
	From: Modestas Vainius <modax@debian.org>
	Subject: Ensure that a type non-smaller than int is passed to va_arg()
	Bug-Debian: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=600445
	Forwarded: no
	Origin: vendor
	Last-Update: 2010-10-21
	
	Variadic arguments are subject to default argument promotion. On some systems,
	mode_t is a short int hence this type cannot be passed to va_arg(). Detect
	such system with autoconf and behave appropriately. Fixes a FTBFS on Debian
	GNU/kFreeBSD.

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge old makefile update (from debian)

2011-01-04  Modestas Vainius  <modax@debian.org>

	Import portable test suite patch from Debian
	
	From: Modestas Vainius <modax@debian.org>
	Description: add portable testsuite which does not depend on linux-only strace.
	Forwarded: no
	Origin: vendor
	Last-Update: 2010-08-06

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge/revert old effort at auto-foo

2011-01-04  Stewart Smith  <stewart@flamingspork.com>

	add missing autoconf files

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge more commits from previous work, makefile related

2011-01-04  Stewart Smith  <stewart@flamingspork.com

	Import improved makefile patch from Debian
	
	From: Modestas Vainius <modax@debian.org>
	Subject: A couple of improvements to Makefile
	Forwarded: no
	Origin: vendor
	Last-Update: 2010-08-05
	
	* Do not use versioned SONAME for libeatmydata. It is not a standard system
	shared library so versioned SONAME has no use for LD_PRELOAD libraries.
	SONAME compatibility symlinks are not need as well so drop them.
	* Support CC, CFLAGS and LDFLAGS variables in the Makefile.
	* Use real targets whenever possible, not dummy ones.

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	merge some previous work, autoconf foo

2011-01-04  Modestas Vainius  <modax@debian.org>

	Apply basic autoconf support (from debian packaging)
	
	From: Modestas Vainius <modax@debian.org>
	Subject: Add basic autoconf support
	Forwarded: no
	Origin: vendor
	Last-Update: 2010-10-20
	
	This patch adds basic autoconf support (configure.in, autogen.sh) to
	libeatmydata.

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	remove a lot of unused m4 macros

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	remove more outdated module stuff from configure.ac (unneeded parts of pandora-build)

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	fix up make distcheck

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	add suppot for LD_PRELOAD equiv on OSX

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	add eatmydata script from debian package

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	bump version number to be greater than any previous release.

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	libeatmydata doesn't have or need pandora plugins

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	bump version number as we've added sync_file_range

2012-07-03  Phillip Susi  <psusi@ubuntu.com>

	add support to wrap sync_file_range, by Phillip Susi. I just modified by adding a wrapping macro for systems without sync_file_range (non-linux)

2012-07-03  Stewart Smith  <stewart@flamingspork.com>

	fix up test case to O_TRUNC and make test file writeable

2012-06-18  Stewart Smith  <stewart@flamingspork.com>

	Updated pandora-build files to version 0.171

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	fix -pedantic warning of ISO C forbids conversion of object pointer to function pointer. We do this by exploiting the fact that we can convert void* to 'any integer' (and using intptr_t as this is provided to be able to hold pointers) and from there we can convert any integer to any pointer.

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	fix 'function declaration isn't a prototype' warning

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	don't need to declare errno

2011-03-17  Stewart Smith  <stewart@flamingspork.com>

	don't LDADD libeatmydata.la

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	update AUTHORS appropriately

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	merge pandora with quickly from monty

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	liblibeatmydata. That's funny.

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	Fixed the makefile.

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	License header.

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	Added in missing quickly files.

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	whoops...

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	use standard COPYING file for license info

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	merge pandora fixes from monty

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	Actually add test_run.sh to EXTRA_DIST

2010-05-25  Stewart Smith  <stewart@flamingspork.com>

	merge pandora fixes from monty

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	Upgraded pandora-build to latest. Added quickly support got make check hooked in properly.

2010-05-24  Monty Taylor  <mordred@inaugust.com>

	Updated pandora-build files to version 0.129

2010-05-24  Stewart Smith  <stewart@flamingspork.com>

	unneeded things in m4/ that shouldn't be in the repo

2010-05-24  Stewart Smith  <stewart@flamingspork.com>

	add missing pandora m4 files

2010-05-24  Stewart Smith  <stewart@flamingspork.com>

	fix up pandora for building libeatmydata properly.

2010-05-24  Stewart Smith  <stewart@flamingspork.com>

	merge pandora-build, minimal configure.ac and Makefile.am prep

2010-05-24  Stewart Smith  <stewart@flamingspork.com>

	merge make dist from Olly Betts

2010-05-23  olly@survex.com

	Add support for "make dist".

2010-03-22  Stewart Smith  <stewart@flamingspork.com>

	update fsynctest.c a bit to check for errors and not try to write to a read only opened file descriptor

2010-03-22  Stewart Smith  <stewart@flamingspork.com>

	update README to reflect other authors

2010-03-22  Olly Betts  <olly@survex.com>

	add makefile for Solaris

2010-03-22  Olly Betts  <olly@survex.com>

	On Solaris, we need to really write data to the file before fsync/fdatasync() does anything.

2010-03-22  Pavel Pushkarev  <paulmd@ya.ru>

	fix some problems with dlsym and threading.

2009-06-26  Stewart Smith  <stewart@flamingspork.com>

	merge CHECK_FILE. Allows eatmydata to only be active iff a file exists

2009-06-25  jobh@broadpark.no

	Support for toggling overrides at runtime through presence of a file.
	
	If CHECK_FILE is defined as a filename, it is checked any time *sync is
	called. If the file is not present, all calls are passed unmodified
	through to libc.
	
	This allows libeatmydata to be turned on systemwide in /etc/ld.so.preload
	and only activated when a specific file such as "/eatmydata" is present.
	Handy for scripting laptop-mode to avoid spinups, temporary latency
	reduction when playing music with slow SSDs, etc.

2009-05-12  Tamas TEVESZ  <ice@extreme.hu>

	add msync() as another way to eat data

2009-02-09  Stewart Smith  <stewart@flamingspork.com>

	In some situations the eatmydata_init() isn't called (latest drizzle tree for some reason).
	
	In open(), just check that libc_open is set to something. if not, do the init.

2009-01-27  Stewart Smith  <stewart@flamingspork.com>

	merge Eric Wong's patches: disable sync, cache dlsym lookup

2009-01-26  Eric Wong  <normalperson@yhbt.net>

	Add wrapper for sync(2)
	
	I test a lot of shell scripts that call sync(1), and
	this provides a significant speedup when running them.

2009-01-26  Eric Wong  <normalperson@yhbt.net>

	memoize dlsym lookup for open(2)
	
	This allows us to avoids the overhead of calling dlsym and error
	checking each time open(2) is called.  This is probably gcc-dependent,
	but I think RTLD_NEXT implies we're on a GNU system anyways.

2008-10-14  Mohsen Hariri  <m.hariri@gmail.com>

	Add fdatasync() wrapper

2008-02-22  Stewart Smith  <stewart@flamingspork.com>

	fixup wrapping of open(2) and testcase

2007-11-27  Stewart Smith  <stewart@flamingspork.com>

	add GPLv3 license

2007-11-27  Stewart Smith  <stewart@flamingspork.com>

	implement support for wrapping open(2) to remove SYNC

2007-11-27  Stewart Smith  <stewart@flamingspork.com>

	added ignore of built files

2007-11-27  Stewart Smith  <stewart@flamingspork.com>

	fsync wrapper

